package com.globo.backstage;

import io.undertow.Undertow;
import io.undertow.client.HttpClient;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.ProxyHandler;
import io.undertow.server.handlers.RequestDumplingHandler;
import io.undertow.server.handlers.SetHeaderHandler;
import io.undertow.util.HeaderValues;
import io.undertow.util.Headers;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.xnio.OptionMap;
import org.xnio.Xnio;
import org.xnio.XnioWorker;

public class Gateway {

  public static void main(String[] args) throws IllegalArgumentException, IOException {

    SetHeaderHandler header = new SetHeaderHandler("Via", "gateway");
    OAuthAuthorization oauth = new OAuthAuthorization(header);
    RequestDumplingHandler dumper = new RequestDumplingHandler(oauth);

    Xnio xnio = Xnio.getInstance();
    XnioWorker worker = xnio.createWorker(new ThreadGroup("ProxyGroup"), OptionMap.EMPTY);
    ProxyHandler proxy = new ProxyHandler(
        HttpClient.create(worker, OptionMap.EMPTY),
        new InetSocketAddress("localhost", 8080)
    );

    ComposableHandler handler = new ComposableHandler(dumper, proxy);

    Undertow server = Undertow.builder()
                      .addListener(8686, "localhost")
                      .setHandler(handler)
                      .setIoThreads(4)
                      .build();
    server.start();
  }

  public static class ComposableHandler implements HttpHandler {
    private final HttpHandler first;
    private final HttpHandler last;

    public ComposableHandler(HttpHandler first, HttpHandler last) {
      this.first = first;
      this.last = last;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
      first.handleRequest(exchange);
      last.handleRequest(exchange);
    }
  }

  public static class OAuthAuthorization implements HttpHandler {
    private final HttpHandler next;

    public OAuthAuthorization(HttpHandler next) {
      this.next = next;
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
      HeaderValues auth = exchange.getRequestHeaders().get(Headers.AUTHORIZATION);
      if(auth == null) {
        exchange.setResponseCode(401);
        exchange.endExchange();
      } else {
        next.handleRequest(exchange);
      }
    }
  }
}