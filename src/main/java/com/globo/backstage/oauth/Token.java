package com.globo.backstage.oauth;

import org.joda.time.DateTime;

public class Token {
  private final String value;
  private final String type;
  private final long duration;

  private final DateTime expires;

  public Token(String value, String type, long duration) {
    this.value = value;
    this.type = type;
    this.duration = duration;
    this.expires = new DateTime().plus(this.duration);
  }

  public boolean expired() {
    return this.expires.isBeforeNow();
  }

  @Override
  public String toString() {
    return String.format("%s: %s", this.type, this.value);
  }
}