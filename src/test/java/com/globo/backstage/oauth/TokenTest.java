package com.globo.backstage.oauth;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;

public class TokenTest {

  @Test
  public void must_be_expired_after_duration() {
    long duration = -10; // negative, duration is in the past
    Token token = new Token("abc", "bearer", duration);

    assertThat(token.expired()).isTrue();
  }

  @Test
  public void must_not_be_expired_after_duration() {
    long duration = 1000;
    Token token = new Token("abc", "bearer", duration);

    assertThat(token.expired()).isFalse();
  }

  @Test
  public void to_string_must_respect_header_format() {
    long duration = 1000;
    Token token = new Token("abc", "bearer", duration);

    assertThat(token.toString()).isEqualTo("bearer: abc");
  }
}
